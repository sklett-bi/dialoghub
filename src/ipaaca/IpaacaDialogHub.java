package ipaaca;

import java.util.EnumSet;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

public class IpaacaDialogHub {

	static OutputBuffer outBuffer = new OutputBuffer("dialoghub");
	/**
	 * set true to output the root mean square from microphone input every 10 ms
	 */
	static boolean vadLog = false;
	/**
	 * recommended to start in parallel: spread -n localhost, maryttsserver,
	 * marytts, diastart
	 */
	public static void main(String[] args) {

		final class MyEventHandler implements HandlerFunctor {
			@Override
			public void handle(AbstractIU iu, IUEventType type, boolean local) {

				Map<String, String> payload = iu.getPayload();

				if (!iu.getCategory().equals("vad") || vadLog)
					System.out.println(iu.getCategory() + "\t: " + payload);

				if (iu.getCategory().equals("asr")) {
					// sending asr output to the dialogmanager
					if (payload.containsKey("transcript")) {
						System.out.println("sending");
						LocalMessageIU localIU = new LocalMessageIU();
						localIU.getPayload().put("transcript", payload.get("transcript"));
						localIU.setCategory("dmIn");
						outBuffer.add(localIU);
					}
				} else if (iu.getCategory().equals("dm")) {
					// sending the decision to the nlg
					if (payload.containsKey("decision")) {
						LocalMessageIU localIU = new LocalMessageIU();
						localIU.getPayload().put("decision", payload.get("decision"));
						localIU.setCategory("nlgIn");
						outBuffer.add(localIU);
					}
				} else if (iu.getCategory().equals("nlg")) {
					// sending nlg-output to tts
					if (payload.containsKey("text")) {
						LocalMessageIU localIU = new LocalMessageIU();
						localIU.getPayload().put("type", "tts.justSpeak");
						localIU.getPayload().put("text", payload.get("text"));
						localIU.setCategory("maryttsrequest");
						outBuffer.add(localIU);
					}
				} else if (iu.getCategory().equals("vad")) {
					if (payload.containsKey("rms")) {
						// use rms (root mean square) information here
					}
				}
			}
		}

		Initializer.initializeIpaacaRsb();
		Set<String> categories = new ImmutableSet.Builder<String>().add("asr", "dm", "nlg", "vad").build();
		InputBuffer inBuffer = new InputBuffer("dialoghub", categories);
		EnumSet<IUEventType> types = EnumSet.allOf(IUEventType.class);
		inBuffer.registerHandler(new IUEventHandler(new MyEventHandler(), types, categories));

	}

}
